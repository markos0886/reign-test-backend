import { ImportDataService } from './services/import-data.service';
import { HttpModule, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { StoryService } from './services/story.service';
import { StorySchema } from './schemas/story.schema';

@Module({
  imports: [
    ConfigModule.forRoot(),
    MongooseModule.forFeature([
      {
        name: 'Story',
        schema: StorySchema,
        collection: 'stories',
      },
    ]),
    MongooseModule.forRoot(process.env.DATABASE_URI),
    HttpModule,
  ],
  controllers: [AppController],
  providers: [ImportDataService, StoryService],
})
export class AppModule {}
