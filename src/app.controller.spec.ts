import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { StoryService } from './services/story.service';
import { getModelToken } from '@nestjs/mongoose';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';

const story: any = {
  storyId: '111111',
  storyTitle: 'Title of any history',
  url: 'http://localhost:3000/111111',
  createdAt: '2021-02-07T21:26:59.913Z',
  isDeleted: false,
  author: 'markos',
};

class StoryModel {
  constructor(private data) {}
  save = jest.fn().mockResolvedValue(this.data);
  static find = () => StoryModel;
  static where = () => StoryModel;
  static sort = () => StoryModel;
  static skip = () => StoryModel;
  static limit = () => StoryModel;
  static exec = jest.fn().mockResolvedValue([story]);
}

describe('App Tests', () => {
  let app: INestApplication;
  let appController: AppController;
  let storyService: StoryService;

  beforeAll(async () => {
    const moduleTest: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [
        StoryService,
        {
          provide: getModelToken('Story'),
          useValue: StoryModel,
        },
      ],
    }).compile();

    storyService = moduleTest.get<StoryService>(StoryService);
    appController = moduleTest.get<AppController>(AppController);
    app = moduleTest.createNestApplication();
    await app.init();
  });

  describe('AppController - Unit Test', () => {
    it('should be defined a controller', () => {
      expect(appController).toBeDefined();
    });

    it('should be defined a service', () => {
      expect(storyService).toBeDefined();
    });

    describe('getAll', () => {
      it('should return an array of stories', async () => {
        const result = await storyService.getAll(0, 20);

        expect(await appController.getAll({ page: 0, limit: 20 })).toBe(result);
      });
    });
  });

  describe('AppController - e2e Test', () => {
    it(`/POST api/all`, async () => {
      const response = await request(app.getHttpServer())
        .post('/api/all')
        .send({ page: 0, limit: 20 })
        .expect(200);
      return expect(response.body).toEqual(await storyService.getAll(0));
    });
  });

  afterAll(async () => {
    await app.close();
  });
});
