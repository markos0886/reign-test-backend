import {
  Controller,
  Delete,
  Res,
  Query,
  HttpStatus,
  Post,
  Body,
  HttpCode,
} from '@nestjs/common';
import { StoryService } from './services/story.service';
import { Filters } from './models/filters.model';
import { Story } from './models/story.model';

@Controller('api')
export class AppController {
  constructor(private readonly appService: StoryService) {}

  @Post('all')
  @HttpCode(200)
  async getAll(@Body() filters: Filters): Promise<Array<Story>> {
    const { page, limit } = filters;
    const data = await this.appService.getAll(page, limit);
    return data;
  }

  @Delete('delete')
  async delete(@Res() res, @Query('id') id: string) {
    const deleted = await this.appService.delete(id);
    return res.status(HttpStatus.OK).json(deleted);
  }
}
