import * as mongoose from 'mongoose';
import * as uniqueValidator from 'mongoose-unique-validator';

export const StorySchema = new mongoose.Schema({
  storyId: { type: String, required: true, unique: true, index: true },
  title: { type: String },
  storyTitle: { type: String },
  url: { type: String },
  storyUrl: { type: String },
  author: { type: String, required: true },
  isDeleted: { type: Boolean, default: false },
  createdAt: { type: Date, default: Date.now },
});

StorySchema.plugin(uniqueValidator);

module.exports = mongoose.model('Story', StorySchema, 'stories');
