import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Story } from 'src/models/story.model';

@Injectable()
export class StoryService {
  constructor(
    @InjectModel('Story') private readonly storyModel: Model<Story>,
  ) {}

  async add(story: any) {
    try {
      const createStory = new this.storyModel(story);
      await createStory.save();
    } catch (err) {
      console.log(
        `Error creating story with ID: ${story.storyId}, already a record with that storyid. ${err.message}`,
      );
    }
  }

  async getAll(page: number, limit = 20): Promise<Story[]> {
    const skip = page * limit;
    return this.storyModel
      .find()
      .where({ isDeleted: false })
      .sort({ createdAt: 'desc' })
      .skip(skip)
      .limit(limit)
      .exec();
  }

  async delete(id: string): Promise<any> {
    return this.storyModel
      .updateOne({ storyId: id }, { isDeleted: true })
      .exec();
  }

  async ckeckEmptyCollection() {
    return this.storyModel.countDocuments().exec();
  }
}
