import { HttpService, Injectable, OnModuleInit } from '@nestjs/common';
import { StoryService } from './story.service';

@Injectable()
export class ImportDataService implements OnModuleInit {
  constructor(
    private storyService: StoryService,
    private httpService: HttpService,
  ) {}

  onModuleInit() {
    this.storyService.ckeckEmptyCollection().then((count) => {
      if (count === 0) {
        this.getImportData();
      }
    });
  }

  async getImportData(page = 0) {
    this.httpService
      .get(`${process.env.URL_TO_IMPORT}&page=${page}`)
      .toPromise()
      .then((res) => {
        this.insertDataInDatabase(res.data, page);
      });
  }

  insertDataInDatabase(data: any, page: number) {
    if (data.hits && data.hits.length > 0) {
      const promises = [];
      for (const item of data.hits) {
        const s = {
          storyId: item.objectID,
          title: item.title || null,
          storyTitle: item.story_title || null,
          url: item.url || null,
          storyUrl: item.story_url || null,
          author: item.author,
          createdAt: new Date(item.created_at),
          isDeleted: false,
        };
        // validation referenced in the doc "Full Stack Test Wireframe"
        // if "story_title" is null, then use "title". If both are null, discard.
        // discarding all records that are incomplete, anticipating that these are saved in db.
        if (s.storyTitle || s.title) {
          promises.push(this.storyService.add(s));
        }
      }
      Promise.all(promises);
    }
    if (data.page !== data.nbPages - 1) {
      this.getImportData(++page);
    }
  }
}
