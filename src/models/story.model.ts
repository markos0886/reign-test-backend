import { Document } from 'mongoose';

export interface Story extends Document {
  _id: string;
  storyId: string;
  title: string;
  storyTitle: string;
  url: string;
  storyUrl: string;
  author: string;
  createdAt: Date;
  isDeleted: boolean;
}
